<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class productTest extends DuskTestCase
{
    public function __construct()
    {
        // $this->user = \App\User::find(1);
    }
    /**
     * Tests if the user can add a new product to his shoppingcart
     *
     * @return void
     */
    public function test_user_can_add_product_to_cart()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/industriele-hanglamp-hazel-ruig-zwart')
                    ->assertSee('Industriële hanglamp Hazel, Ruig zwart')
                    ->click('In Winkelwagen')
                    ->assertSee('Mijn winkelwagen')
                    ->assertSee('Industriële hanglamp Hazel');
        });
    }

    /**
     * Tests if user is able to checkout the product he has selected
     *
     * @return void
     */
    public function test_user_can_checkout_product()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/checkout/cart/')
                    ->assertSee('Industriële hanglamp Hazel')
                    ->click('Ga door naar afrekenen')
                    ->pause(1000)
                    ->assertSee('E-mail')
                    ->type('username', $this->user->email)
                    ->click('Volgende')
                    ->pause(100)
                    ->assertSee('Bezorgadres')
                    ->click($this->user->aanhef)
                    ->type('firstname', $this->user->voornaam)
                    ->type('lastname', $this->user->achternaam)
                    ->type('experius_postcode_postcode', $this->user->postcode)
                    ->type('experius_postcode_housenumber', $this->user->huisnr)
                    ->type('street[0]', $this->user->adres)
                    ->type('city', $this->user->plaats)
                    ->type('city', $this->user->plaats)
                    ->click('Volgende')
                    ->assertSee('Betaalmethode')
                    ->click('iDEAL')
                    ->click('ING');
        });
    }
}
