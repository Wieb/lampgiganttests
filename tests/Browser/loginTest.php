<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class loginTest extends DuskTestCase
{
    /**
     * Test User cannot login with wrong credentials
     *
     * @return void
     */
    public function test_user_cannot_login_with_wrong_credentials()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('customer/account/login')
                    ->assertSee('Geregistreerde klanten')
                    ->type('email', 'ihavefailed@gmail.com')
                    ->type('login[username]', 'password1')
                    ->pause(1000)
                    ->assertSee('De account aanmelding is onjuist of uw account is tijdelijk uitgeschakeld. Wacht en probeer het later opnieuw.');
        });
    }
    /**
     * Test User can login with correct credentials
     * Would only work after register test has been run
     * @return void
     */
    // public function test_user_can_login_with_correct_credentials()
    // {
    //     $user = \App\User::find(1);
    //     $this->browse(function (Browser $browser) {
    //         $browser->visit('customer/account/login')
    //                 ->assertSee('Geregistreerde klanten')
    //                 ->type('email', 'ihavefailed@gmail.com')
    //                 ->type('login[username]', 'password1')
    //                 ->pause(1000)
    //                 ->assertSee('De account aanmelding is onjuist of uw account is tijdelijk uitgeschakeld. Wacht en probeer het later opnieuw.');
    //     });
    // }
}
